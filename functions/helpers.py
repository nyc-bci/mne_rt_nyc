import numpy as np
import pandas as pd
import itertools

def rename_and_exclude_channels(info, ch_names):
    new_ch_names = []
    for ch in info.ch_names:
        c_lowered = False
        new_ch = ''
        if ch in ['FP1', 'FPZ', 'FP2', 'FZ', 'FCZ', 'CZ', 'CPZ', 'PZ', 'POZ', 'CB1', 'OZ', 'CB2']:
            for c in ch[::-1]:
                if not c_lowered and c.isalpha():
                    new_ch += c.lower()
                    c_lowered = True
                else:
                    new_ch += c 
            new_ch_names.append((ch, new_ch[::-1]))
    
    exclude = ['FPZ', 'CB1', 'CB2']
    new_ch_names = dict([ex for ex in new_ch_names if ex[0] not in exclude])
    
    return new_ch_names
    
def visualize_fn(ax, im, psds_pre, freqs, picks):
    cmap = 'RdBu_r'
    im = ax.imshow(psds_pre.T, aspect='auto',
                   origin='lower', cmap=cmap)
    ax.set_yticks(np.arange(0, len(freqs), 4))
    ax.set_yticklabels(freqs[::4].round(1))
    ax.set_ylabel('Frequency (Hz)')
    ax.set_xticks(np.arange(0, len(picks), 2))
    ax.set_xticklabels(picks[::2])
    ax.set_xlabel('EEG channel index')
    im.set_clim()
    return im

def print_online_psds(psds_means, psds, freqs, ii, n_samples):
    print("PSD example value from " + str(ii*n_samples) + " to " + str((ii+1)*n_samples) + " (ch 0, band 0): " + str(psds[0,0]))
    print("psds means from " + str(ii*n_samples) + " to " + str((ii+1)*n_samples))
    print(psds_means)
    print()
    print("SUCCESS!")
    print("RESULT SHAPE (nodes, bands)")
    print(psds_means.shape)

def calc_stats(x, d, stats):
    if stats == 1:
        for ch in x:
            std = np.std(d[0, ch, :])
            mean = np.mean(d[0, ch, :])
            print(f'channel: {ch}-> {mean} (+/-) {std}')

def calc_stats2(x, d, stats):
    if stats == 2:
        for ch in x:
            std = np.std(d[:, ch, 0])
            mean = np.mean(d[:, ch, 0])
            std2 = np.std(d[:, ch, 1])
            mean2 = np.mean(d[:, ch, 1])
            print(f'channel alpha: {ch}-> {mean} (+/-) {std}')
            print(f'channel beta: {ch}-> {mean2} (+/-) {std2}')

def calc_stats3(d, stats):
    if stats == 3:
        std = np.std(d[:, 0])
        mean = np.mean(d[:, 0])
        std2 = np.std(d[:, 1])
        mean2 = np.mean(d[:, 1])
        std3 = np.std(d[:, d.shape[1]-1])
        mean3 = np.mean(d[:, d.shape[1]-1])
        print(f'channel alpha: -> {mean} (+/-) {std}')
        print(f'channel beta: -> {mean2} (+/-) {std2}')
        print(f'channel2 beta: -> {mean3} (+/-) {std3}')

#visualize "ch x freq bin" plot for unscaled
def box_plot(raw, y, X_):
    freq = ["a", "b", "c", "d", "e"]
    descriptors = ["".join(tp) for tp in list(itertools.product(raw.ch_names,freq))]
    X_flat = X_.flatten()
    X_pre_df = []
    for i in range(len(X_flat)):
        X_pre_df.append([X_flat[i], descriptors[i%5], y[int(i/100)]])
    X_df = pd.DataFrame(data=X_pre_df, columns=["psd mean", "ch x freq bin", "arousal"])
    return X_df


def high_low_mapping(x):
    return 'low' if x <= 5 else 'high'
