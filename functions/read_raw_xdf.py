import mne
import numpy as np
import os
import matplotlib.pyplot as plt
import json

def read_raw_xdf(fname, stream_id, *args, **kwargs):
    """Read XDF file.

    Parameters
    ----------
    fname : str
        Name of the XDF file.
    stream_id : int
        ID (number) of the stream to load.

    Returns
    -------
    raw : mne.io.Raw
        XDF file data.

    dig_montage: mne.dig_montage from channel locations in file.
    """
    from pyxdf import load_xdf, match_streaminfos, resolve_streams

    streams, header = load_xdf(fname)

    dig_montage = None
    for stream in streams:
        print("STREAM_ID: " + str(stream["info"]["stream_id"]))
        
        if stream["info"]["stream_id"] == stream_id:
            
            channel_pos = stream["info"]["desc"][0]["channels"][0]["channel"]
            if channel_pos:
                channel_pos = {i["label"][0]: [float(i["location"][0]["X"][0]), float(i["location"][0]["Y"][0]), float(i["location"][0]["Z"][0])]  for i in channel_pos}
                dig_montage = mne.channels.make_dig_montage(channel_pos)
            break  # stream found

    fs = float(stream["info"]["nominal_srate"][0])
    n_chans = int(stream["info"]["channel_count"][0])
    #print(stream["info"])
    labels, types, units = [], [], []
    try:
        for ch in stream["info"]["desc"][0]["channels"][0]["channel"]:
            labels.append(str(ch["label"][0]))
            if ch["type"]:
                types.append(ch["type"][0])
            if ch["unit"]:
                units.append(ch["unit"][0])
    except (TypeError, IndexError):  # no channel labels found
        pass
    if not labels:
        labels = [str(n) for n in range(n_chans)]
    if not units:
        units = ["NA" for _ in range(n_chans)]
    info = mne.create_info(ch_names=labels, sfreq=fs, ch_types="eeg")
    # convert from microvolts to volts if necessary
    scale = np.array([1e-6 if u == "microvolts" else 1 for u in units])
    raw = mne.io.RawArray((stream["time_series"] * scale).T, info)
    raw._filenames = [fname]
    first_samp = stream["time_stamps"][0]
    markers = match_streaminfos(resolve_streams(fname), [{"type": "Markers"}])
    for stream_id in markers:
        for stream in streams:
            if stream["info"]["stream_id"] == stream_id:
                break
        onsets = stream["time_stamps"] - first_samp
        descriptions = [item for sub in stream["time_series"] for item in sub]
        raw.annotations.append(onsets, [0] * len(onsets), descriptions)
    return raw, dig_montage
