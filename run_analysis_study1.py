#!/usr/bin/env python
# coding: utf-8

from pyxdf import load_xdf, match_streaminfos, resolve_streams
import imblearn
from imblearn.over_sampling import SMOTE
from imblearn.under_sampling import RandomUnderSampler 
from sklearn.model_selection import StratifiedKFold, train_test_split, cross_val_score
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.metrics import accuracy_score, plot_confusion_matrix
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
import random
from collections import Counter
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import seaborn as sns

import mne

from analysis import analysis
from features import psd_means
from functions.read_raw_xdf import read_raw_xdf
from functions.helpers import *

########################################
########### PARAMS #####################
########################################

# NOTE: sample rate needs to stay consistent for now

RANDOM_STATE = 0
PSD_MEANS = 'PSD_MEANS'

# data

STREAM_TYPE = 'EEG'                                             # default value: extract EEG stream from .xdf file
STREAM_ID = 1                                                   # default value: extract EEG stream from .xdf file 

# deap

BASELINE_LENGTH_SEC = 5                                         # if 0 -> then no baseline correction is applied
TRIGGER_LENGTH = 60.0  

# preprocessing                     

autoreject = False                                              # activate autoreject

# analysis

SELECTION = None                                                # channels to include from EEG recording
EXCLUDE_CHANNELS = ['Fp1', 'Fp2', 'Pz', 'FC1']                  # channels to exlude from EEG 
LABEL_NAME = 'AVG_Arousal'                                      # individual arousal rating after video stimuli, # options: arousal_slider.response ,  AVG_Arousal                
EPOCH_LENGTH_SEC = 1                                            # length of an extracted segment
LAST_STIMULI_REJECT = 1                                         # (hacky) last film stimuli reaction was not recorded in full length

# ml

CLF_OVERSAMPLING = False                                        # classification oversampling with SMOTE
CLF_EVENT_OUT = True
MINMAX_SCALING = False                                          # MinMax Scaling input for classification algorithm
STANDARD_SCALING = True

# log

VERBOSE = 1                                                     # verbose statement controls output printing level, if 0 no output
STATS = 3                                                       # show certain stats
FREQ_BOX_PLOTS = 1                                              # show freq boxplots

# files

DATADIR =  'data/study2_files/'                                 # directory where data (.xdf, and metadata) is stored

FILENAME_EEG_LIST = ['041220_1656.xdf',
                     '041220_1741.xdf',
                     '041220_1826.xdf'
                     ]           
FILENAME_Y_LIST = ['_deap_study_2020_Dez_04_1656.csv',
                   '_deap_study_2020_Dez_04_1741.csv', 
                   '_deap_study_2020_Dez_04_1826.csv'
                   ]

##############################################
########### READ IN EVENTS ###################
##############################################

# Preprocess and extract PSD segments for data in all files

for file_counter, filename_eeg in enumerate(FILENAME_EEG_LIST):

    if VERBOSE == 1:
        print(f'file_counter: {file_counter}, filename_eeg: {filename_eeg}, filename_y: {FILENAME_Y_LIST[file_counter]}')
    
    # Reading file and montage

    raw, dig_montage = read_raw_xdf(fname=DATADIR + filename_eeg, stream_id=STREAM_ID)
    raw.set_montage(dig_montage)

    # (hacky) Preparing for autoreject

    if autoreject:
        new_ch_names = rename_and_exclude_channels(raw.info, raw.info.ch_names)
        print("---------")
        print("new_ch_names")
        print(new_ch_names)
        mne.rename_channels(raw.info, new_ch_names)

    # Picking channels

    if SELECTION is not None:
        raw = raw.pick_types(raw.info, eeg=True, selection = SELECTION )
    else:
        raw = raw.pick_types(raw.info, eeg=True, exclude = EXCLUDE_CHANNELS )
    
    if VERBOSE == 1:
        print(f'channels selected: {raw.ch_names}')

    # Creating events from lsl trigger annotations

    fs = int(raw.info['sfreq'])
    events_from_annot, event_dict = mne.events_from_annotations(raw)

    # (Optional) baseline correction

    if BASELINE_LENGTH_SEC == 0:
        epochs = mne.Epochs(raw, events_from_annot, 2, -0, TRIGGER_LENGTH, baseline=None) 
    else:
        epochs = mne.Epochs(raw, events_from_annot, 2, -1.0*BASELINE_LENGTH_SEC,
                             TRIGGER_LENGTH, baseline=(-1.0*BASELINE_LENGTH_SEC, 0)) 

    ########### READ IN LABELS ###################

    df_l = pd.read_csv(DATADIR + 'psychopy_data/'+ FILENAME_Y_LIST[file_counter], header = 0)
    df_l = df_l.iloc[1:,:] #routine 0 removed
    label = np.asarray(df_l.loc[:, LABEL_NAME])

    ################ ITERATE THROUGH EPOCHS ##################

    X, y = [], []

    for i in range( epochs.selection.shape[0]-LAST_STIMULI_REJECT):
        epoch_arr = epochs[i].get_data()

        if VERBOSE and STATS > 0:
            print("epoch: " + str(i) + ", raw stats")
            chs = range(epoch_arr.shape[1])
            calc_stats(chs, epoch_arr, STATS)

        # Prepare data for cropping

        SHOCK_MOMENT = 2000 #remove 4 seconds right after stimuli
        epoch_arr_stimulus = epoch_arr[0, :, BASELINE_LENGTH_SEC*fs+SHOCK_MOMENT:(BASELINE_LENGTH_SEC*fs+60*fs)]
        
        max_n_crop = int(  epoch_arr_stimulus.shape[1] / (fs) / EPOCH_LENGTH_SEC) 
        
        if VERBOSE:
            print(f'max_n_crop: {max_n_crop}') #maximum number of segments that can be extracted from one video stimuli
        
        cropped_arr = []

        start, count = 0,0

        # Crop epochs of video watching period (~60 sec) into EPOCH_LENGTH_SEC segments

        while( count < max_n_crop-2 ):
            stop = start + (fs) * EPOCH_LENGTH_SEC

            cr = epoch_arr_stimulus[:, int(start):int(stop)]
            cropped_arr.append(cr)
            start = stop
            count += 1
        cropped_arr_ = np.asarray(cropped_arr)
        
        # TODO Need preprocessing here

        # Compute PSD means (ch x bands) for every epoch extracted

        stim_epochs = mne.EpochsArray(cropped_arr_, raw.info)

        psds_means_segment, psds_total, freqs_total = analysis.offline_extract_features(stim_epochs, raw, feature = PSD_MEANS)

        if VERBOSE and STATS > 0:
            print("psds_means_segment.shape")
            print(psds_means_segment.shape)
            print("epoch: " + str(i) + ", psds stats")
            chs = range(psds_means_segment.shape[1])
            calc_stats2(chs, psds_means_segment, STATS)

        psds_means_segments = np.reshape( psds_means_segment, (-1, psds_means_segment.shape[1]* psds_means_segment.shape[2]))
        psds_means_segments = np.reshape(psds_means_segments, (-1, psds_means_segments.shape[1]))
        
        X.append( psds_means_segments )
        y_append = np.repeat(label[i], psds_means_segments.shape[0])
        y.append(y_append)
        
 
    X = np.asarray(X)
    y = np.asarray(y)
    y = np.reshape(y, y.shape[0]* y.shape[1])
    x = []
    for column in range(X.shape[2]):
        z = np.ravel(X[:,:, column])
        x.append(z)
    x = np.asarray(x)
    X = np.swapaxes(x, 0,1) 

    if file_counter == 0:
        X_list, y_list = X, y
    else:
        X_list = np.concatenate([X_list, X])
        y_list = np.concatenate([y_list, y])

##############################################
########### DEFINE AROUSAL LABELS ############
##############################################

# Binarizing targets

hlm = np.vectorize(high_low_mapping)
y_a = hlm(y_list)

# Balancing data

if CLF_OVERSAMPLING:
    oversample = SMOTE()
    X_, y = oversample.fit_resample(X_list, y_a)
else:
    X_ = X_list
    y = y_a

if CLF_EVENT_OUT is True and CLF_OVERSAMPLING is False:
    rus = RandomUnderSampler(sampling_strategy = 'majority', random_state=RANDOM_STATE)
    X_, y = rus.fit_resample(X_, y)
    print('Resampled dataset shape %s' % Counter(y))

#X_ = np.clip(X_, -10, 18)
#plt.title("Alpha PSDs for all channels for all observations")
#plt.plot(range(X_.shape[0]), X_[::, ::20])

##############################################
############## FEATURE SCALING ###############
##############################################

X_unscaled = X_
if MINMAX_SCALING:
    scaler = MinMaxScaler()
    X_ = scaler.fit_transform(X_)

if VERBOSE and STATS > 0:
    print("Min Max Scaling feature stats:")
    calc_stats3(X_, STATS)

if STANDARD_SCALING:
    scaler = StandardScaler()
    X_ = scaler.fit_transform(X_)

if VERBOSE and STATS > 0:
    print("Standard Scaling feature stats:")
    calc_stats3(X_, STATS)

if FREQ_BOX_PLOTS:
    plt.rcParams.update({'font.size': 4})
    X_df = box_plot(raw, y, X_unscaled)
    #fig = plt.figure()
    sns.boxplot(x="ch x freq bin", y="psd mean", hue="arousal", data=X_df)
    plt.title('Comparing mean psd values for all obervation (unscaled) ')
    plt.show()

    X_df = box_plot(raw, y, X_)
    #fig = plt.figure()
    sns.boxplot(x="ch x freq bin", y="psd mean", hue="arousal", data=X_df)
    plt.title('Comparing mean psd values for all obervation (scaled) ')
    plt.show()
    plt.rcParams.update({'font.size': 10})


##############################################
########### CLASSIFY AROUSAL STATES ##########
##############################################

clf = SVC()
#clf = RandomForestClassifier(max_depth=2, random_state=RANDOM_STATE)
#define holdout set
X_train, X_holdout, y_train, y_holdout = train_test_split(X_, y, test_size = 0.2, random_state = RANDOM_STATE)

#do cross validation on training set only -> then we have train, test and holdout
#scores = cross_val_score(clf, X_train, y_train, cv=10, fit_params= {class_weight:"balanced"})
#print(f'scores: {scores} \n Accuracy: {round( scores.mean(), 4) }  (+/- {round( scores.std() * 2, 4) })')
#print('this gives us an unbiased 10-fold estimate of the prediction power on unseen data')

clf.fit(X_train, y_train)
print('\nholdout classification')
plot_confusion_matrix(clf, X_holdout, y_holdout)  # doctest: +SKIP
#plt.show()
#plt.clf()
print('holdout accuracy')
print( accuracy_score(y_holdout, clf.predict(X_holdout), normalize=True) )
#plt.hist(y_holdout)


skf = StratifiedKFold(n_splits=10,  shuffle=True)
for train_index, test_index in skf.split(X_train, y_train):#
    X_m, X_test = X_train[train_index], X_train[test_index]
    y_m, y_test = y_train[train_index], y_train[test_index]
    clf.fit(X_m, y_m)
    #plot_confusion_matrix(clf, X_test, y_test)  # doctest: +SKIP
    #plt.show()
    #print( accuracy_score(y_test, clf.predict(X_test), normalize=True) )
    #plt.hist(y_m)

#do cross validation on training set only -> then we have train, test and holdout
scores = cross_val_score(clf, X_train, y_train, cv=10)                       #fit_params= {class_weight:"balanced"}
print(f'scores: {scores} \n Accuracy: {round( scores.mean(), 4) }  (+/- {round( scores.std() * 2, 4) })')

##############################################
plt.title('Confusion matrix: arousal classification on holdout data with SVM')
plt.show()


