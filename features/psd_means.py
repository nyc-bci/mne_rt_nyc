from mne.time_frequency import psd_welch, tfr_morlet
import numpy as np

def psd_means(params, epoch, chan_idxs, fs, show=False):
    fmin, fmax = 1., params["FREQ_MASK"]
    eeg_bands = params["EEG_BANDS"]

    psds, freqs = psd_welch(epoch, fmin=fmin, fmax=fmax, n_fft=params["PSD_N_FFT"], n_per_seg=params["PSD_N_PER_SEG"],n_overlap=params["PSD_N_OVERLAP"],verbose='WARNING') #samples needs to be > n_overlap; overlap=50
    
    psds = 10. * np.log10(psds)

    if len(psds.shape) == 3:
        psds = psds[0, :, :] #for online only

    psds_means = np.zeros((psds.shape[0], len(eeg_bands))) #psds.shape[0] should contain all the eegs nodes for which psd worked
   
    if show:
        print("psds shape (nodes, freqs)")
        print(psds.shape)
        print()
        print("freqs")
        print(freqs)
        print()
        print("psds means shape (valid nodes/bands)")
        print(psds_means.shape)
        print()
        
    for c_idx in range(0, psds_means.shape[0]):
        for f, band in enumerate(eeg_bands): 
            freq_ixs = np.where((freqs >= eeg_bands[band][0]) & 
                           (freqs <= eeg_bands[band][1]))[0]
            psds_means[c_idx, f] = psds[c_idx,freq_ixs].flatten().mean(0) #:,

    return psds_means, psds, freqs