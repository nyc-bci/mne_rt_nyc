# Modified based on 
# mne tutorial "Overview of MEG/EEG analysis with MNE-Python",
# mne_realtime tutorial "Plot real-time epoch data with LSL client",
# mne_realtime tutorial "Compute real-time power spectrum density with FieldTrip client"

import os, sys, getopt
import os.path as op
import subprocess
import time

import numpy as np
import matplotlib.pyplot as plt

import mne
from mne.time_frequency import psd_welch
from mne.utils import running_subprocess

from mne_realtime import FieldTripClient
from mne_realtime import LSLClient
from mne.time_frequency import psd_welch, tfr_morlet
from autoreject import AutoReject, get_rejection_threshold
import random
import pandas as pd
import scipy.io

from sklearn import svm
import pickle

from features import psd_means
from functions.read_raw_xdf import read_raw_xdf
from functions.helpers import *

# Log settings

mne.set_log_level('CRITICAL')

# Global vars

fig = None
ax = None
fig, ax = plt.subplots(1)
ar = AutoReject()

################################################
################### PARAMS #####################
###############################################

# Available feature extractors

PSD_MEANS = 'PSD_MEANS'

# Feature params

FREQ_MASK = 41.                         #upper filter freq limit
#https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3812603/
EEG_BANDS       =  {  
                    'Delta' : (1., 3),  #0.5 officially
                    'Theta' : (4, 7),
                    'Alpha' : (8, 13),  #8.5, 12.5
                    'Beta'  : (14, 30),
                    'Gamma' : (31, FREQ_MASK) #50 officially
                    }
PSD_MEANS_PARAMS                       = {} 
PSD_MEANS_PARAMS["PSD_N_FFT"]          = 500   #fft window size 
PSD_MEANS_PARAMS["PSD_N_PER_SEG"]      = 500   #samples per psd segment must be <= PSD_N_FFT
PSD_MEANS_PARAMS["PSD_N_OVERLAP"]      = 0     #must be < PSD_N_PER_SEG
PSD_MEANS_PARAMS["FREQ_MASK"]          = FREQ_MASK
PSD_MEANS_PARAMS["EEG_BANDS"]          = EEG_BANDS
                    
# Epoch lengths (for fixed length splitting)

SEGMENT_LENGTH  = 2.0   # segment length
SEGMENT_OVERLAP = 0.5   # segment overlap
EPOCH_T_MIN     = -0.01 # epoch start point in segment
EPOCH_T_MAX     = 2.0   # epoch end point in segment


def offline_extract_features(epochs, raw, visualize = False, feature=PSD_MEANS):
    chan_idxs = [raw.ch_names.index(ch) for ch in raw.ch_names]

    fs = raw.info['sfreq']
    event_count = len(epochs.selection)

    show = False
    psds_means_segment, psds_total, freqs_total = None, None, None

    picks = epochs.ch_names
    vis_steps = 5
    im = None

    for i, epoch in enumerate(epochs.iter_evoked()):

        # psd means

        if feature is PSD_MEANS:

            psds_means, psds, freqs = psd_means.psd_means(PSD_MEANS_PARAMS, epoch, chan_idxs, fs, show)

            if visualize and (i % vis_steps == 0):
                if im is None:
                    im = visualize_fn(ax, im, psds[:, freqs < FREQ_MASK], freqs, picks)
                else:
                    im.set_data(psds[:, freqs < FREQ_MASK].T)

                plt.title('continuous power spectrum (t = %0.2f sec to %0.2f sec)'
                    % (EPOCH_T_MIN, EPOCH_T_MAX), fontsize=10)
                plt.pause(1)

            if psds_means_segment is None:
                psds_means_segment = [psds_means]
                psds_total = [psds]
                freqs_total = [freqs]

            psds_means_segment = np.concatenate((psds_means_segment, [psds_means]), axis=0)
            
            psds_total = np.concatenate((psds_total, [psds]), axis=0)
            freqs_total = np.concatenate((freqs_total, [freqs]), axis=0)

        # other

        else:
            print("Feature extractor not available")
        
    return psds_means_segment, psds_total, freqs_total

def offline_preprocessing(raw):
    raw = raw.filter(1, FREQ_MASK, fir_design='firwin')
    raw = raw.notch_filter(50.)
    return raw

def offline_analysis(raw, visualize, autoreject=False, montagetype='standard_1020'):
    
    # Set montage for autoreject

    if autoreject:
        if raw.get_montage() is None:
            montage = mne.channels.make_standard_montage(montagetype)
            raw.set_montage(montage)
        print()
        print("bci montage: ")
        print(raw.get_montage())
        print()

    # Fixed length splitting (assuming provided stream is of same label)

    events = mne.make_fixed_length_events(raw, duration=SEGMENT_LENGTH, overlap=SEGMENT_OVERLAP)
    
    # Reject criteria

    reject_criteria = dict(
                  #eeg=1000, # V (EEG channels)
                  )

    # Preprocessing

    raw = offline_preprocessing(raw)

    chan_idxs = [raw.ch_names.index(ch) for ch in raw.ch_names]
    
    raw.set_eeg_reference('average', projection=True) #common average referencing

    epochs = mne.Epochs(raw, events, tmin=EPOCH_T_MIN, tmax=EPOCH_T_MAX,
                        reject=reject_criteria, preload=True)
    
    epochs_clean = epochs

    # Clean and repair epochs

    if autoreject:
        try:
            ar.fit(epochs)
            epochs_clean = ar.transform(epochs)
            print()
            print("Autoreject successful. Change log settings to see more details.")
            print()
        except Exception as inst:
            print(inst)

    # Final Feature extraction

    return offline_extract_features(epochs_clean, raw, visualize, feature = PSD_MEANS)

def online_analysis(host, wait_max, speedup, visualize, picks, online_length, n_samples):

    with LSLClient(info=None, host=host, wait_max=wait_max) as rt_client: #raw.info
        
        print(rt_client.info)

        n_samples = int(n_samples)
        fs = int(rt_client.info["sfreq"])

        time.sleep(n_samples / fs)  # make sure at least one epoch is available

        chan_idxs = [rt_client.info.ch_names.index(ch) for ch in rt_client.info.ch_names]

        im = None

        # Iterate through time intervals of n_samples/fs

        for ii in range(int(online_length/(n_samples / fs))):

            print()
            print("---------")
            print(str(ii) + " Iteration")

            # Fetch latest n_samples

            epoch = rt_client.get_data_as_epoch(n_samples=n_samples,
                                                picks=picks)
            if picks is not None:
                chan_idxs = [epoch.ch_names.index(ch) for ch in picks]

            print(epoch)

            # Feature extraction

            show = True

            try:
                psds_means, psds, freqs = psd_means.psd_means(PSD_MEANS_PARAMS, epoch, chan_idxs, fs, show)
            except Exception as inst:
                print(inst)

            print_online_psds(psds_means, psds, freqs, ii, n_samples)

            if visualize:
                tmin = epoch.events[0][0] / fs
                tmax = (epoch.events[0][0] + n_samples) / fs

                psds_avg = np.mean(psds, axis=0) #avg over all electrodes ,so it could be printed against time

                try:
                    if ii == 0:
                        im = visualize_fn(ax, im, psds[:, freqs < FREQ_MASK], freqs, picks or epoch.ch_names)
                    else:
                        im.set_data(psds[:, freqs < FREQ_MASK].T)

                    plt.title('continuous power spectrum (%0.2f samples in %0.2f sec)'
                              % (n_samples, (n_samples / fs)), fontsize=10)

                except Exception as inst:
                            print("ERROR!")
                            print(inst)

                plt.pause(0.5 / speedup)

            # Live prediction

            if False:

                ### TODO Feature scaling

                # Load (offline-learned) classifier model

                clf_model = pickle.load(open(MODEL_PATH, 'rb'))

                #Reshape data for classification input: -> flatten all dimension but axis = 0
                
                preprocessed_arr = np.reshape(psds_means, (psds_means.shape[0], -1))

                prediction = clf_model.predict(preprocessed_arr)
                print(f'prediction: {prediction}')


