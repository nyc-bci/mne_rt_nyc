import os, sys, getopt
import os.path as op

import mne

from analysis import analysis
from functions.read_raw_xdf import read_raw_xdf
from functions.helpers import *

################################################
#################### ARGS ######################
###############################################

MODE = 0                # -m
FILE = None             # -f
VISUALIZE = False       # -v
AUTOREJECT = False      # -a

################################################
#################### DIRS ######################
###############################################

user = "p388849" or "davidbethge"
SEED_PATH = '/Users/'+user+'/Documents/Porsche/Projects/data_collection/SEED_IV'
MODEL_PATH = 'models/happy_neutral_sad_classifier.sav'

# EEG channel picks

PICKS            = [None] * 5
PICKS[0]         = None #['EEG 001', 'EEG 002', 'EEG 003', 'EEG 004', 'EEG 005', 'EEG 006', 'EEG 007', 'EEG 008', 'EEG 009', 'EEG 010', 'EEG 011', 'EEG 012', 'EEG 013', 'EEG 014', 'EEG 015', 'EEG 016', 'EEG 017', 'EEG 018', 'EEG 019', 'EEG 020', 'EEG 021', 'EEG 022', 'EEG 023', 'EEG 024', 'EEG 025', 'EEG 026', 'EEG 027', 'EEG 028', 'EEG 029', 'EEG 030', 'EEG 031', 'EEG 032'] 
PICKS[1]         = None #['Fp1', 'Fp2', 'F3', 'F4', 'C3', 'C4', 'P3', 'P4', 'O1', 'O2', 'F7', 'F8', 'T7', 'T8', 'P7', 'P8', 'Fz', 'Cz', 'Pz', 'M1', 'M2', 'AFz', 'CPz', 'POz']
PICKS[2]         = None #['Fp1', 'Fp2', 'F3', 'F4', 'C3', 'C4', 'P3', 'P4', 'O1', 'O2', 'F7', 'F8', 'T7', 'T8', 'P7', 'P8', 'Fz', 'Cz', 'Pz', 'M1', 'M2', 'AFz', 'CPz', 'POz']
PICKS[3]         = None #['Fp1', 'Fp2'] #only frontal lobe features
PICKS[4]         = None

################################################
################### PARAMS #####################
###############################################

# Online study params

HOST = 'eeg'            # this is the host id that identifies your stream on LSL
WAIT_MAX = 10           # this is the max wait time in seconds until client connection
SPEEDUP = 10            # speedup wait time between epochs. how much faster than 0.5s
ONLINE_LENGTH   = 10    # entire study length
N_SAMPLES       = 100   # single epoch length to be analysed

################################################
###################### MAIN ####################
###############################################

def main(argv):
    visualize = VISUALIZE
    mode = MODE
    file = FILE
    autoreject = AUTOREJECT

    inputfile = ''
    outputfile = ''
    try:
      opts, args = getopt.getopt(argv,"hm:f:va",["help=","mode=","file=","viz==","autoreject=="])
    except getopt.GetoptError as err:
      print(err)
      print('main.py -m <mode (0 - off - .xdf, 1 - off - .fif, 2 - on > -f <inputfile> -v (visualize) -a (autoreject)')
      sys.exit(2)
    for opt, arg in opts:
      if opt in ("-h", "--help"):
        print('main.py -m <mode (0 - off - .xdf, 1 - off - .fif, 2 - on > -f <inputfile> -v (visualize) -a (autoreject)')
        sys.exit()
      elif opt in ("-m", "--mode"):
         mode = int(arg)
      elif opt in ("-f", "--file"):
         file = arg
      elif opt in ("-v", "--viz"):
            visualize = True
      elif opt in ("-a", "--autoreject"):
            autoreject = True

    print('Mode: ', mode)
    print('Input file:', 'Default' if file is None else file)
    print('Visualize:', visualize)
    print('Autoreject:', autoreject)
    print("---------")
    print()

    ##### FILE LOADING #####

    ### OFFLINE from .xdf

    if mode is 1:
        fname = "data/121120_105213sad.xdf";

        if not file:
            file = fname
        
        print("Loading " + file)

        raw, dig_montage = read_raw_xdf(file, "EEG")
        raw.set_montage(dig_montage)

    ### OFFLINE from .fif

    elif mode is 2:
        sample_data_folder = mne.datasets.sample.data_path()
        sample_data_raw_file = os.path.join(sample_data_folder, 'MEG', 'sample',
                                            'sample_audvis_filt-0-40_raw.fif')
        if not file:
            file = sample_data_raw_file
        
        print("Loading " + file)

        raw = mne.io.read_raw_fif(sample_data_raw_file)
        raw = raw.load_data()
    
    ### OFFLINE from .bdf deap

    elif mode is 3:
        fname = "data/deap/s01.bdf";

        if not file:
            file = fname
        
        print("Loading " + file)

        raw = mne.io.read_raw_bdf(file, preload=True, exclude=['EXG1', 'EXG2', 'EXG3', 'EXG4', 'EXG5', 'EXG6', 'EXG7', 'EXG8', 'GSR1', 'GSR2', 'Erg1', 'Erg2', 'Resp', 'Plet', 'Temp'])

    ### OFFLINE from .mat SEED_IV

    elif mode is 4:

        # SEED_IV

        session = 1
        data_dir = SEED_PATH + '/eeg_raw_data/' + str(session)+'/'

        # One participant & One session

        filename = os.listdir(data_dir)[0]
        
        channel_order = pd.read_excel(SEED_PATH + '/Channel Order.xlsx', sheet_name=0, header = None)
        channel_names = channel_order[0].values

        mat = scipy.io.loadmat(data_dir + filename)
        trial_names = [x for x in mat.keys() if 'eeg' in x]
        session1_label = [1,2,3,0,2,0,0,1,0,1,2,1,1,1,2,3,2,2,3,3,0,3,0,3]
        
        mne_info = mne.create_info(ch_names = channel_names.tolist(), sfreq = 200.0, ch_types='eeg', verbose=None)

        only_use_alpha, pick_only_FP_channels = True, False

        i = 0

        name = trial_names[0]
        raw_dat = mat[name]
        label = session1_label[i] 

        raw = mne.io.RawArray(mat[name], info = mne_info)

        # (hacky) Prepare for autoreject

        if autoreject:
            new_ch_names = rename_and_exclude_channels(raw.info, raw.info.ch_names)
            print("---------")
            print("new_ch_names")
            print(new_ch_names)
            mne.rename_channels(raw.info, new_ch_names)
 
    ##### ANALYSIS #####

    ### ONLINE

    if mode is 0: 
        analysis.online_analysis(HOST, WAIT_MAX, SPEEDUP, visualize, PICKS[mode], ONLINE_LENGTH, N_SAMPLES)

    ### OFFLINE

    if mode > 0:

        # Prepare for auto reject

        if autoreject:
            exclude = ['FPZ', 'CB1', 'CB2']
            print("['FPZ', 'CB1', 'CB2'] are excluded by default in case autoreject is on")
        else:
            exclude = []

        # Pick channels

        raw = raw.pick_types(raw.info, eeg = True, selection=PICKS[mode] or None, exclude=exclude)

        # Offline analysis (currently returning only psd features)

        psds_segments, psds, freqs = analysis.offline_analysis(raw, visualize=visualize, autoreject=autoreject)
        
        print()
        print("---------")
        print("SUCCESS!")
        print("RESULT SHAPE (segments, nodes, bands)")
        print(psds_segments.shape)
        print()
        
if __name__ == "__main__":
   print()
   print("The new modes are: -m 0 online, 1 .xdf, 2 .fif, 3 .bdf, 4 .mat SEED_IV")
   print()
   main(sys.argv[1:])
