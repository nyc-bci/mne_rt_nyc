# MNE RT NYC

Repo containing a pipeline for online and offline eeg analyis for arousal state prediction. 

[Pipeline overview](https://www.figma.com/file/aiOP5XmF0sxMbOUvw0AemL/Affective-BCI-share?node-id=0%3A1)

- `.xdf`: Raw EEG, trigger events

- `.csv`: Stimuli information, ratings

## DIRS
```bash
- experimentation/
- functions/		# Helper functions
- features/		# Feature extractors
- models/		# Trained arousal predictors
- analysis/		# Main offline and online analysis functionality
```

## INSTALL
1. make a python venv with python3.7
2. `pip install -r requirements.txt`

## RUN
-m is mode (online: **0** / offline **1** (.xdf) , **2** (.fif),  **3** (.bdf),  **4** (.mat from SEED_IV))

-f is eeg filepath

-v visualizes psd diagram over eeg channels per epoch (*untested)

-a activates autoreject (*untested)

### Online
#### session 1
```sh
python run_analysis.py -m 0
```
returns a tensor of (nodes, bands) for the current epoch

#### session 2
```
python mock_eeg_lsl_streamer.py # mock lsl client, sending noisy sinus waves
```

### Offline
```sh
python run_analysis.py -m 1 -f <i.e. path/to/data/121120_10521happy.xdf file>
python run_analysis.py -m 2 -f <empty or path/to/data/*.fif file> 
```
returns a tensor of (segments, nodes, bands) 

## HYPERPARAMS
To be explained
